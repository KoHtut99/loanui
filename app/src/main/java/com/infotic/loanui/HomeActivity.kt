package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.infotic.loanui.databinding.ActivityHomeBinding
import com.infotic.loanui.menuactivities.MenuActivity
import com.infotic.loanui.navifragments.CalculatorFragment
import com.infotic.loanui.navifragments.HistoryFragment
import com.infotic.loanui.navifragments.HomeFragment
import com.infotic.loanui.navifragments.HomeFragment2
import com.infotic.loanui.navifragments.HomeLv2Fragment
import com.infotic.loanui.navifragments.LoanFragment
import com.infotic.loanui.navifragments.PaymentFragment

class HomeActivity : AppCompatActivity() {

    private lateinit var binding : ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.menuIcon.setOnClickListener {
            val i = Intent(this@HomeActivity, MenuActivity::class.java)
            startActivity(i)
        }

        binding.notiIcon.setOnClickListener {
            val i = Intent(this@HomeActivity, NotificationActivity::class.java)
            startActivity(i)
        }

        replaceFragment(HomeFragment2())

        binding.bnvMain.setOnItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    replaceFragment(HomeLv2Fragment())
                    binding.tvFragmentTitle.visibility = View.GONE
                    binding.ivLM.visibility = View.VISIBLE
                }
                R.id.loan -> {
                    replaceFragment(LoanFragment())
                    binding.ivLM.visibility = View.GONE
                    binding.tvFragmentTitle.visibility = View.VISIBLE
                    binding.tvFragmentTitle.text = "Loan"
                }
                R.id.calculator -> {
                    replaceFragment(CalculatorFragment())
                    binding.ivLM.visibility = View.GONE
                    binding.tvFragmentTitle.visibility = View.VISIBLE
                    binding.tvFragmentTitle.text = "Calculator"

                }
                R.id.payment -> {
                    replaceFragment(PaymentFragment())
                    binding.ivLM.visibility = View.GONE
                    binding.tvFragmentTitle.visibility = View.VISIBLE
                    binding.tvFragmentTitle.text = "Payment"
                }
                R.id.history -> {
                    replaceFragment(HistoryFragment())
                    binding.ivLM.visibility = View.GONE
                    binding.tvFragmentTitle.visibility = View.VISIBLE
                    binding.tvFragmentTitle.text = "History"
                }
            }
            true
        }

    }

    private fun replaceFragment(fragment : Fragment) {
        val fragmentManager : FragmentManager = supportFragmentManager
        val transient : FragmentTransaction = fragmentManager.beginTransaction()
        transient.replace(R.id.fcvMain, fragment)
        transient.addToBackStack(null)
        transient.commit()
    }
}