package com.infotic.loanui

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.Button
import com.infotic.loanui.databinding.ActivityUpgradeLevel2Binding

class UpgradeLevel2Activity : AppCompatActivity() {
    private lateinit var binding : ActivityUpgradeLevel2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpgradeLevel2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnUpgradeLevel2.setOnClickListener {
            showCustomDialogBox()
        }


    }

    private fun showCustomDialogBox() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.awaiting_approval_dialogbox)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()

        val btnBackToHome : Button = dialog.findViewById(R.id.btnBackToHome)

        btnBackToHome.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }
}