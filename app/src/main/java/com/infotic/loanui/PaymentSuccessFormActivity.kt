package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivityPaymentSuccessFormBinding

class PaymentSuccessFormActivity : AppCompatActivity() {
    private lateinit var binding : ActivityPaymentSuccessFormBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentSuccessFormBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnBackToHome.setOnClickListener {
            startActivity(Intent(this@PaymentSuccessFormActivity, HomeActivity::class.java))
        }
    }
}