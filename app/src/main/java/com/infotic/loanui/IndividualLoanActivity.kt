package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivityIndividualLoanBinding
import com.infotic.loanui.loanforms.FirstLoanFormActivity

class IndividualLoanActivity : AppCompatActivity() {
    private lateinit var binding : ActivityIndividualLoanBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIndividualLoanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnApplyLoan.setOnClickListener {
            startActivity(Intent(this@IndividualLoanActivity, FirstLoanFormActivity::class.java))
        }
    }
}