package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivityOtpVerificationBinding

class OTP_Verification_Activity : AppCompatActivity() {
    private lateinit var binding : ActivityOtpVerificationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnVerify.setOnClickListener {
            val i = Intent(this@OTP_Verification_Activity, LoginActivity::class.java)
            startActivity(i)
        }
    }
}