package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivitySetNewPasswordBinding

class SetNewPasswordActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySetNewPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySetNewPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSavePsw.setOnClickListener {
            val i = Intent(this@SetNewPasswordActivity, LoginActivity::class.java)
            startActivity(i)
        }
    }
}