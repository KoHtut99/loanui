package com.infotic.loanui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.infotic.loanui.adapters.NotiData
import com.infotic.loanui.adapters.ShowNotiAdapter
import com.infotic.loanui.databinding.ActivityNotificationBinding

class NotificationActivity : AppCompatActivity() {

    private lateinit var binding : ActivityNotificationBinding
    private var notiData : List<NotiData> = listOf()
    private lateinit var showNotiAdapter: ShowNotiAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        recyclerSetUp()
    }

    private fun recyclerSetUp() {

        notiData = listOf(
            NotiData(R.drawable.approve_logo, "Approved Loan", "Congratulation , we are approve  you request loan amount 3,955,095 MMk .", "22/06/2023", "10:00 AM"  ),
            NotiData(R.drawable.reject_logo, "Rejected Loan", "Sorry , we are reject  you request loan amount 500,000,000 MMk .", "22/06/2023", "10:00 AM"  ),
        )

        showNotiAdapter = ShowNotiAdapter(notiData)
        binding.rvNoti.adapter = showNotiAdapter
        binding.rvNoti.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
    }
}