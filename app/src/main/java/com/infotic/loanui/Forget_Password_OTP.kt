package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivityForgetPasswordOtpBinding


class Forget_Password_OTP : AppCompatActivity() {
    private lateinit var binding : ActivityForgetPasswordOtpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnVerify.setOnClickListener {
            val i = Intent(this@Forget_Password_OTP, SetNewPasswordActivity::class.java)
            startActivity(i)
        }
    }
}