package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.adapters.CalendarData
import com.infotic.loanui.adapters.RepaymentCalendarAdapter
import com.infotic.loanui.databinding.ActivityRepaymentCalendarClickBinding

class RepaymentCalendarClickActivity : AppCompatActivity(),
    RepaymentCalendarAdapter.OnClickCalendar {
    private var calendarData: List<CalendarData> = listOf()
    private lateinit var repaymentCalendarAdapter : RepaymentCalendarAdapter
    private lateinit var binding : ActivityRepaymentCalendarClickBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepaymentCalendarClickBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnNextPayment.setOnClickListener {
            startActivity(Intent(this@RepaymentCalendarClickActivity, RepaymentActivity::class.java))
        }

        setUpRv()
    }

    private fun setUpRv() {
        calendarData = listOf(
            CalendarData(R.drawable.calendar_black_logo, "07/08", R.drawable.btn_white),
            CalendarData(R.drawable.calendar_gold_add_logo, "07/09",R.drawable.btn_blue),
            CalendarData(R.drawable.calendar_black_add_logo, "07/10",R.drawable.btn_white),
            CalendarData(R.drawable.calendar_black_add_logo, "07/11",R.drawable.btn_white),
        )

        repaymentCalendarAdapter = RepaymentCalendarAdapter(calendarData, this)
        binding.rvCalendar.adapter = repaymentCalendarAdapter
    }

    override fun onClickCalendar(position: Int) {

    }
}