package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class LoanRvAdapter(private var loanData : List<LoanData>, private var onClickLoanItems: OnClickLoanItems) : RecyclerView.Adapter<LoanRvAdapter.LoanViewHolder>() {

    interface OnClickLoanItems{
        fun OnClickLoanItems(position: Int)
    }

    class LoanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv : ImageView = itemView.findViewById(R.id.iv)
        val tvTitle : TextView = itemView.findViewById(R.id.tv1)
        val tvAmt : TextView = itemView.findViewById(R.id.tv2)
        val btnDetail : Button = itemView.findViewById(R.id.btnDetail)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoanViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.home_rv_items, parent, false)
        return LoanViewHolder(view)
    }

    override fun getItemCount(): Int {
        return loanData.size
    }

    override fun onBindViewHolder(holder: LoanViewHolder, position: Int) {
        val currentItem = loanData[position]
        holder.iv.setImageResource(currentItem.iv)
        holder.tvTitle.text = currentItem.tvTitle
        holder.tvAmt.text = currentItem.tvAmt
        holder.btnDetail.setOnClickListener {
            onClickLoanItems.OnClickLoanItems(position)
        }
    }
}

data class LoanData(
    var iv: Int,
    var tvTitle: String,
    var tvAmt: String
)