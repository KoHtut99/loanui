package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class HistoryRvAdapter(private var historyData : List<HistoryData>, private var onClickLoanItems: OnClickLoanItems) : RecyclerView.Adapter<HistoryRvAdapter.HistoryViewHolder>() {

    interface OnClickLoanItems{
        fun OnClickLoanItems(position: Int)
    }

    class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv : ImageView = itemView.findViewById(R.id.iv)
        val tvTitle : TextView = itemView.findViewById(R.id.tv1)
        val btnDetail : Button = itemView.findViewById(R.id.btnDetail)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.home_rv_items, parent, false)
        return HistoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return historyData.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val currentItem = historyData[position]
        holder.iv.setImageResource(currentItem.iv)
        holder.tvTitle.text = currentItem.tvTitle
        holder.btnDetail.setBackgroundResource(currentItem.btnDrawableColor)
        holder.btnDetail.setTextColor(ContextCompat.getColor(holder.itemView.context, currentItem.btnTextColor))
        holder.btnDetail.text = currentItem.btnText

        holder.btnDetail.setOnClickListener {
            onClickLoanItems.OnClickLoanItems(position)
        }
    }
}

data class HistoryData(
    val iv: Int,
    val tvTitle: String,
    val btnText : String,
    val btnTextColor : Int,
    val btnDrawableColor: Int,
    val status : String

)