package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class ShowNotiAdapter(private var notiData: List<NotiData>) : RecyclerView.Adapter<ShowNotiAdapter.NotiViewHolder>() {

    class NotiViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivNoti : ImageView = itemView.findViewById(R.id.ivNotiLoan)
        var tvTitle : TextView = itemView.findViewById(R.id.tvNotiTitle)
        var tvMsg : TextView = itemView.findViewById(R.id.tvNotiMsg)
        var tvDate : TextView = itemView.findViewById(R.id.tvDate)
        var tvTime : TextView = itemView.findViewById(R.id.tvTime)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotiViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.shownoti_layout, parent, false)
        return NotiViewHolder(view)
    }

    override fun getItemCount(): Int {
        return notiData.size
    }

    override fun onBindViewHolder(holder: NotiViewHolder, position: Int) {
        val currentItem = notiData[position]
        holder.ivNoti.setImageResource(currentItem.ivNoti)
        holder.tvTitle.text = currentItem.tvTitle
        holder.tvMsg.text = currentItem.tvMessage
        holder.tvDate.text = currentItem.tvDate
        holder.tvTime.text = currentItem.tvTime
    }
}

data class NotiData(
    var ivNoti: Int,
    var tvTitle: String,
    var tvMessage: String,
    var tvDate: String,
    var tvTime: String
)