package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class RepaymentCalendarAdapter(private var calendarData: List<CalendarData>, private var onClickCalendar: OnClickCalendar) : RecyclerView.Adapter<RepaymentCalendarAdapter.CalendarViewHolder>() {

    interface OnClickCalendar{
        fun onClickCalendar(position: Int)
    }

    inner class CalendarViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val ivCalendar : ImageView = itemView.findViewById(R.id.ivCalendar)
        val tvDate : TextView = itemView.findViewById(R.id.tvDate)
        val cvCalendar : CardView = itemView.findViewById(R.id.cvCalendar)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cv_calendar_layout, parent, false)
        return CalendarViewHolder(view)
    }

    override fun getItemCount(): Int {
        return calendarData.size
    }

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        val currentItems = calendarData[position]
        holder.ivCalendar.setImageResource(currentItems.ivCalendar)
        holder.tvDate.text = currentItems.tvDate
        holder.cvCalendar.setBackgroundResource(currentItems.cvDrawableColor)
        holder.cvCalendar.setOnClickListener {
            onClickCalendar.onClickCalendar(position)
        }
    }
}

data class CalendarData(
     var ivCalendar : Int,
     var tvDate : String,
     val cvDrawableColor: Int
)


