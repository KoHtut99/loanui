package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class HomeRv2Adapter(private var homeRv2Data: List<HomeRv2Data>, private var onClickHome2Items: OnClickHome2Items) : RecyclerView.Adapter<HomeRv2Adapter.HomeRv2ViewHolder>() {

    interface OnClickHome2Items{
        fun OnClickHome2Items(position: Int)
    }
    inner class HomeRv2ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val iv : ImageView = itemView.findViewById(R.id.ivI)
        val tvDate : TextView = itemView.findViewById(R.id.tvRepaymentDate)
        val tvAmtMMk : TextView = itemView.findViewById(R.id.tvAmtMMK)
        val btnRepayment : Button = itemView.findViewById(R.id.btnRepayment)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeRv2ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.payment_rv_items, parent , false)
        return HomeRv2ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return homeRv2Data.size
    }

    override fun onBindViewHolder(holder: HomeRv2ViewHolder, position: Int) {
        val currentItems = homeRv2Data[position]
        holder.iv.setImageResource(currentItems.iv)
        holder.tvDate.text = currentItems.tvDate
        holder.tvAmtMMk.text = currentItems.tvAmtMMk
        holder.btnRepayment.setBackgroundResource(currentItems.btnDrawableColor)
        holder.btnRepayment.setOnClickListener {
            onClickHome2Items.OnClickHome2Items(position)
        }
    }
}

data class HomeRv2Data(
    val iv : Int,
    val tvDate : String,
    val tvAmtMMk : String,
    val btnDrawableColor: Int
)