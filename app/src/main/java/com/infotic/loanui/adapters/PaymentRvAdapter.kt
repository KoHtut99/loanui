package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class PaymentRvAdapter(private var paymentData: List<PaymentData>, private var onClickPaymentItems: OnClickPaymentItems) : RecyclerView.Adapter<PaymentRvAdapter.PaymentViewHolder>() {
    interface OnClickPaymentItems{
        fun OnClickPaymentItems(position: Int)
    }

    inner class PaymentViewHolder(itemView: View) :  RecyclerView.ViewHolder(itemView){
        val tvDate : TextView = itemView.findViewById(R.id.tvRepaymentDate)
        val tvAmtMMk : TextView = itemView.findViewById(R.id.tvAmtMMK)
        val btnRepayment : Button = itemView.findViewById(R.id.btnRepayment)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.payment_rv_items, parent , false)
        return PaymentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return paymentData.size
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        val currentItems = paymentData[position]
        holder.tvDate.text = currentItems.tvDate
        holder.tvAmtMMk.text = currentItems.tvDate
        holder.btnRepayment.setBackgroundResource(currentItems.btnDrawableColor)
        holder.btnRepayment.setOnClickListener {
            onClickPaymentItems.OnClickPaymentItems(position)
        }
    }
}

data class PaymentData(
    val tvDate : String,
    val tvAmtMMk : String,
    val btnDrawableColor: Int
)