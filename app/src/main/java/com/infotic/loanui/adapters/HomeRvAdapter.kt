package com.infotic.loanui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class HomeRvAdapter(private var loanData: List<LoanData>, private var onClickHomeItems: HomeRvAdapter.OnClickHomeItems) : RecyclerView.Adapter<HomeRvAdapter.HomeViewHolder>() {

    interface OnClickHomeItems{
        fun OnClickHomeItems(position: Int)
    }

    class HomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv : ImageView = itemView.findViewById(R.id.iv)
        val tvTitle : TextView = itemView.findViewById(R.id.tv1)
        val tvAmt : TextView = itemView.findViewById(R.id.tv2)
        val btnDetail : Button = itemView.findViewById(R.id.btnDetail)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.home_rv_items, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return loanData.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val currentItem = loanData[position]
        holder.iv.setImageResource(currentItem.iv)
        holder.tvTitle.text = currentItem.tvTitle
        holder.tvAmt.text = currentItem.tvAmt
        holder.btnDetail.setOnClickListener {
            onClickHomeItems.OnClickHomeItems(position)
        }
    }
}