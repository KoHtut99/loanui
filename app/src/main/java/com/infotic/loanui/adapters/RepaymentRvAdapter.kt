package com.infotic.loanui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.R

class RepaymentRvAdapter(private var repaymentData : List<RepaymentData>) : RecyclerView.Adapter<RepaymentRvAdapter.RepaymentViewHolder>() {

    var selectedPosition =RecyclerView.NO_POSITION
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    inner class RepaymentViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val ivCheck : ImageView = itemView.findViewById(R.id.ivCheck)
        val paymentLogo : ImageView = itemView.findViewById(R.id.paymentLogo)
        val paymentName : TextView = itemView.findViewById(R.id.paymentName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepaymentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.payment_items, parent, false)
        return RepaymentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return repaymentData.size
    }

    override fun onBindViewHolder(holder: RepaymentViewHolder, position: Int) {
        val currentItems = repaymentData[position]

        holder.ivCheck.setImageResource(currentItems.ivCheck)
        holder.paymentLogo.setImageResource(currentItems.paymentLogo)
        holder.paymentName.text = currentItems.paymentName
        holder.itemView.setOnClickListener {
            selectedPosition = holder.adapterPosition
        }

        if (position == selectedPosition){
            holder.itemView.setBackgroundResource(R.drawable.payment_selected_bg)
            holder.ivCheck.setImageResource(R.drawable.bluechecked_logo)
        }else{
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
            holder.ivCheck.setImageResource(R.drawable.checkedmark_logo)
        }



    }
}

data class RepaymentData(
    val ivCheck : Int,
    val paymentLogo : Int,
    val paymentName : String
)