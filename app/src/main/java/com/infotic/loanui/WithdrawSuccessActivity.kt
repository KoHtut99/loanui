package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivityWithdrawSuccessBinding

class WithdrawSuccessActivity : AppCompatActivity() {
    private lateinit var binding : ActivityWithdrawSuccessBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWithdrawSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnBackToHome.setOnClickListener {
            startActivity(Intent(this@WithdrawSuccessActivity, HomeActivity::class.java))
        }
    }
}