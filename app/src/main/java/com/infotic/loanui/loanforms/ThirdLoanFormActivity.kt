package com.infotic.loanui.loanforms

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.infotic.loanui.FiveDotsAnimationActivity
import com.infotic.loanui.HomeActivity
import com.infotic.loanui.R
import com.infotic.loanui.UpgradeLevel2Activity
import com.infotic.loanui.databinding.ActivityThirdLoanFormBinding

class ThirdLoanFormActivity : AppCompatActivity() {
    private lateinit var binding : ActivityThirdLoanFormBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityThirdLoanFormBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnApply.setOnClickListener {
            startActivity(Intent(this@ThirdLoanFormActivity, FiveDotsAnimationActivity::class.java))
        }
        showAwaitDialog()
    }

    private fun showAwaitDialog(){
        val getKey = intent.getStringExtra("key")
        if (getKey == "ShowDialog") {
            awaitingApprovalDialogBox()
        }
    }

        private fun awaitingApprovalDialogBox() {
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.awaiting_approval_dialogbox)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            val btnBackToHome : Button = dialog.findViewById(R.id.btnBackToHome)

            btnBackToHome.setOnClickListener {
                startActivity(Intent(this, HomeActivity::class.java))
            }
    }
}