package com.infotic.loanui.loanforms

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.R
import com.infotic.loanui.databinding.ActivitySecondLoanFormBinding

class SecondLoanFormActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySecondLoanFormBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondLoanFormBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnContinue.setOnClickListener {
            startActivity(Intent(this@SecondLoanFormActivity, ThirdLoanFormActivity::class.java))
        }
    }
}