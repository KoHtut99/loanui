package com.infotic.loanui.loanforms

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.R
import com.infotic.loanui.databinding.ActivityFirstLoanFormBinding

class FirstLoanFormActivity : AppCompatActivity() {
    private lateinit var binding : ActivityFirstLoanFormBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirstLoanFormBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnContinue.setOnClickListener {
            startActivity(Intent(this@FirstLoanFormActivity, SecondLoanFormActivity::class.java))
        }


    }
}