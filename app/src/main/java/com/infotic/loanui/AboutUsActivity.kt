package com.infotic.loanui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class AboutUsActivity : AppCompatActivity() {
    private lateinit var backIcon : ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        backIcon = findViewById(R.id.backIcon)
        backIcon.setOnClickListener { onBackPressed() }
    }
}