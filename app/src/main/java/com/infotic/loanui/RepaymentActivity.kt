package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.adapters.CalendarData
import com.infotic.loanui.adapters.RepaymentCalendarAdapter
import com.infotic.loanui.adapters.RepaymentData
import com.infotic.loanui.adapters.RepaymentRvAdapter
import com.infotic.loanui.databinding.ActivityRepaymentBinding

class RepaymentActivity : AppCompatActivity(), RepaymentCalendarAdapter.OnClickCalendar{

    private lateinit var binding : ActivityRepaymentBinding
    private var calendarData: List<CalendarData> = listOf()
    private var repaymentData : List<RepaymentData> = listOf()
    private lateinit var repaymentRvAdapter: RepaymentRvAdapter
    private lateinit var repaymentCalendarAdapter : RepaymentCalendarAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepaymentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        setUpRv1()
        setUpRv2()

        binding.btnMakeRepayment.setOnClickListener {
            startActivity(Intent(this@RepaymentActivity, FiveDotsAnimationActivity::class.java))
        }

    }

    private fun setUpRv1() {
        calendarData = listOf(
            CalendarData(R.drawable.calendar_black_logo, "07/08", R.drawable.btn_white),
            CalendarData(R.drawable.calendar_gold_add_logo, "07/08", R.drawable.btn_blue),
            CalendarData(R.drawable.calendar_black_add_logo, "07/08", R.drawable.btn_white),
            CalendarData(R.drawable.calendar_black_add_logo, "07/08", R.drawable.btn_white)
        )

        repaymentCalendarAdapter = RepaymentCalendarAdapter(calendarData, this)
        binding.rvCalendar.adapter = repaymentCalendarAdapter
    }

    private fun setUpRv2() {
        repaymentData = listOf(
            RepaymentData(R.drawable.checkedmark_logo, R.drawable.kbzpay_logo, "KBZ Pay"),
            RepaymentData(R.drawable.checkedmark_logo, R.drawable.cbpay_logo, "CB Pay"),
            RepaymentData(R.drawable.checkedmark_logo, R.drawable.mpu_logo, "MPU"),
            RepaymentData(R.drawable.checkedmark_logo, R.drawable.wavepay_logo, "Wave Pay"),
            RepaymentData(R.drawable.checkedmark_logo, R.drawable.kbz_logo, "KBZ Bank"),
            RepaymentData(R.drawable.checkedmark_logo, R.drawable.ayabank_logo, "AYA Bank")
        )

        repaymentRvAdapter = RepaymentRvAdapter(repaymentData)
        binding.rvPayment.adapter = repaymentRvAdapter
    }

    override fun onClickCalendar(position: Int) {
        startActivity(Intent(this@RepaymentActivity, RepaymentCalendarClickActivity::class.java))
    }

}