package com.infotic.loanui

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.infotic.loanui.databinding.ActivityHomeBinding
import com.infotic.loanui.databinding.ActivitySmeloanBinding

class SMELoanActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySmeloanBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySmeloanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnApplyLoan.setOnClickListener {
            showCustomDialogBox()
        }
    }

    private fun showCustomDialogBox() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.upgrade_to_lv2_dialogbox)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()

        val btnSureUpgradeLevel2: Button = dialog.findViewById(R.id.btnSureUpgradeLevel2)
        val tvCancel: TextView = dialog.findViewById(R.id.tvCancel)

        btnSureUpgradeLevel2.setOnClickListener {
            startActivity(Intent(this, UpgradeLevel2Activity::class.java))
        }

        tvCancel.setOnClickListener {
            dialog.dismiss()
        }
    }
}