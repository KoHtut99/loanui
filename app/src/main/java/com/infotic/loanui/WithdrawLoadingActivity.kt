package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.infotic.loanui.databinding.ActivityWithdrawLoadingBinding
import com.infotic.loanui.loanforms.ThirdLoanFormActivity

class WithdrawLoadingActivity : AppCompatActivity() {

    private lateinit var binding : ActivityWithdrawLoadingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWithdrawLoadingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler().postDelayed( {
            val intent = Intent(this@WithdrawLoadingActivity, WithdrawSuccessActivity::class.java)
            intent.putExtra("key2", "paymentSuccess")
            startActivity(intent)
        }, 2000)
    }
}