package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.databinding.ActivitySplashScreenBinding

class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.setOnClickListener {
            val i = Intent(this@SplashScreenActivity, RegisterActivity::class.java)
            startActivity(i)
        }

        binding.tvLogin.setOnClickListener {
            val i = Intent(this@SplashScreenActivity, LoginActivity::class.java)
            startActivity(i)
        }
    }
}