package com.infotic.loanui.navifragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.infotic.loanui.R
import com.infotic.loanui.RepaymentActivity
import com.infotic.loanui.adapters.PaymentData
import com.infotic.loanui.adapters.PaymentRvAdapter
import com.infotic.loanui.databinding.FragmentPaymentBinding

class PaymentFragment : Fragment(), PaymentRvAdapter.OnClickPaymentItems {
    private lateinit var binding: FragmentPaymentBinding
    private var paymentData : List<PaymentData> = listOf()
    private lateinit var paymentRvAdapter: PaymentRvAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPaymentBinding.inflate(inflater, container, false)
        setUpRv()
        return binding.root
    }

    private fun setUpRv() {
        paymentData = listOf(
            PaymentData("8/7/2023", "329,591 MMK", R.drawable.btn_blue),
            PaymentData("8/8/2023", "329,591 MMK", R.drawable.btn_midgray),
            PaymentData("8/9/2023", "329,591 MMK", R.drawable.btn_midgray)
        )
        paymentRvAdapter = PaymentRvAdapter(paymentData, this)
        binding.rvPayment.adapter = paymentRvAdapter
        binding.rvPayment.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    }

    override fun OnClickPaymentItems(position: Int) {
        startActivity(Intent(requireContext(), RepaymentActivity::class.java))
    }


}