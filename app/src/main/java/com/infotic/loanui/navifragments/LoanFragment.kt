package com.infotic.loanui.navifragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.infotic.loanui.IndividualLoanActivity
import com.infotic.loanui.R
import com.infotic.loanui.adapters.LoanData
import com.infotic.loanui.adapters.LoanRvAdapter
import com.infotic.loanui.databinding.FragmentLoanBinding

class LoanFragment : Fragment(), LoanRvAdapter.OnClickLoanItems {

    private lateinit var binding : FragmentLoanBinding
    private var loanData : List<LoanData> = listOf()
    private lateinit var loanRvAdapter : LoanRvAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoanBinding.inflate(inflater, container, false)
        setUpRv()
        return binding.root
    }

    private fun setUpRv() {
        loanData = listOf(
            LoanData(R.drawable.briefcase_logo, "Individual Loan", "Maximum - 50,000,000 MMK"),
            LoanData(R.drawable.blue_sme_logo, "SME Loan", "Maximum - 300,000,000 MMK "),
            LoanData(R.drawable.red_livestock_logo, "Livestock Loan", "Maximum - 500,000,000 MMK"),
            LoanData(R.drawable.green_agricultural_logo, "Agricultural Loan", "Maximum - 500,000,000 MMK ")
        )

        loanRvAdapter = LoanRvAdapter(loanData, this)
        binding.rvLoan.adapter = loanRvAdapter
        binding.rvLoan.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

    }

    override fun OnClickLoanItems(position: Int) {
        startActivity(Intent(requireContext(), IndividualLoanActivity::class.java))
    }


}