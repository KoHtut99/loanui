package com.infotic.loanui.navifragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.infotic.loanui.ApprovedActivity
import com.infotic.loanui.R
import com.infotic.loanui.adapters.HistoryData
import com.infotic.loanui.adapters.HistoryRvAdapter
import com.infotic.loanui.adapters.ViewPagerAdapter
import com.infotic.loanui.databinding.FragmentHistoryBinding
class HistoryFragment : Fragment(), HistoryRvAdapter.OnClickLoanItems {
    private lateinit var binding: FragmentHistoryBinding
    private var historyData: List<HistoryData> = listOf()
    private lateinit var historyRvAdapter: HistoryRvAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHistoryBinding.inflate(inflater, container, false)

        setBtnAllColor()
        setUpRv()


        binding.btnAll.setOnClickListener {
            setBtnAllColor()
            setUpRv()
        }

        binding.btnApproved.setOnClickListener {
            filterItems("approved")
            setApprovedBtnColor()
        }

        binding.btnPending.setOnClickListener {
            filterItems("pending")
            setPendingBtnColor()
        }

        binding.btnReject.setOnClickListener {
            filterItems("rejected")
            setRejectedBtnColor()
        }

        return binding.root

    }
    private fun setBtnAllColor() {
        binding.btnAll.setBackgroundResource(R.drawable.btn_blue)
        binding.btnAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        binding.btnApproved.setBackgroundResource(R.drawable.btn_white)
        binding.btnApproved.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnPending.setBackgroundResource(R.drawable.btn_white)
        binding.btnPending.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnReject.setBackgroundResource(R.drawable.btn_white)
        binding.btnReject.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
    }

    private fun setApprovedBtnColor() {
        binding.btnAll.setBackgroundResource(R.drawable.btn_white)
        binding.btnAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnApproved.setBackgroundResource(R.drawable.btn_blue)
        binding.btnApproved.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        binding.btnPending.setBackgroundResource(R.drawable.btn_white)
        binding.btnPending.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnReject.setBackgroundResource(R.drawable.btn_white)
        binding.btnReject.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
    }

    private fun setPendingBtnColor() {
        binding.btnAll.setBackgroundResource(R.drawable.btn_white)
        binding.btnAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnApproved.setBackgroundResource(R.drawable.btn_white)
        binding.btnApproved.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnPending.setBackgroundResource(R.drawable.btn_blue)
        binding.btnPending.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        binding.btnReject.setBackgroundResource(R.drawable.btn_white)
        binding.btnReject.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
    }

    private fun setRejectedBtnColor(){
        binding.btnAll.setBackgroundResource(R.drawable.btn_white)
        binding.btnAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnApproved.setBackgroundResource(R.drawable.btn_white)
        binding.btnApproved.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnPending.setBackgroundResource(R.drawable.btn_white)
        binding.btnPending.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
        binding.btnReject.setBackgroundResource(R.drawable.btn_blue)
        binding.btnReject.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
    }

    private fun filterItems(status : String) {
        val filteredItemData = historyData.filter { it.status.equals(status, ignoreCase = true) }
        historyRvAdapter = HistoryRvAdapter(filteredItemData, this)
        binding.rvHistory.adapter = historyRvAdapter
        historyRvAdapter.notifyDataSetChanged()
    }

    private fun setUpRv() {
        historyData = listOf(
            HistoryData(
                R.drawable.briefcase_logo,
                "Individual Loan",
                "Approved",
                R.color.btn_approved_text_color,
                R.drawable.btn_approved_bg,
                "approved"
            ),
            HistoryData(
                R.drawable.blue_sme_logo,
                "SME Loan",
                "Reject",
                R.color.btn_reject_text_color,
                R.drawable.btn_reject_bg,
                "rejected"
            ),

            HistoryData(
                R.drawable.red_livestock_logo,
                "Livestock Loan",
                "Pending",
                R.color.btn_pending_text_color,
                R.drawable.btn_pending_bg,
                "pending"
            ),

            HistoryData(
                R.drawable.green_agricultural_logo,
                "Agricultural Loan",
                "Approved",
                R.color.btn_approved_text_color,
                R.drawable.btn_approved_bg,
                "approved"
            )
        )

        historyRvAdapter = HistoryRvAdapter(historyData, this)
        binding.rvHistory.adapter = historyRvAdapter
        binding.rvHistory.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    }

    override fun OnClickLoanItems(position: Int) {
        startActivity(Intent(requireContext(), ApprovedActivity::class.java))
    }
}