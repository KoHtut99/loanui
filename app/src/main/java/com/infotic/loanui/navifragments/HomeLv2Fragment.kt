package com.infotic.loanui.navifragments

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.findViewTreeViewModelStoreOwner
import androidx.recyclerview.widget.LinearLayoutManager
import com.infotic.loanui.R
import com.infotic.loanui.SMELoanActivity
import com.infotic.loanui.UpgradeLevel2Activity
import com.infotic.loanui.adapters.HomeRvAdapter
import com.infotic.loanui.adapters.LoanData
import com.infotic.loanui.databinding.FragmentHomeLv2Binding

class HomeLv2Fragment : Fragment(), HomeRvAdapter.OnClickHomeItems {
    private var loanData : List<LoanData> = listOf()
    private lateinit var homeRvAdapter : HomeRvAdapter
    private lateinit var binding : FragmentHomeLv2Binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeLv2Binding.inflate(inflater, container, false)

        binding.btnUpgradeLevel2.setOnClickListener {
            showCustomDialogBox()
        }

        setUpRv()
        return binding.root
    }

    private fun showCustomDialogBox() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.upgrade_to_lv2_dialogbox)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()

        val btnSureUpgradeLevel2 : Button = dialog.findViewById(R.id.btnSureUpgradeLevel2)
        val tvCancel : TextView = dialog.findViewById(R.id.tvCancel)

        btnSureUpgradeLevel2.setOnClickListener {
            startActivity(Intent(requireContext(), UpgradeLevel2Activity::class.java))
        }

        tvCancel.setOnClickListener{
            dialog.dismiss()
        }

    }

    private fun setUpRv() {
        loanData = listOf(
            LoanData(R.drawable.briefcase_logo, "Individual Loan", "Maximum - 50,000,000 MMK"),
            LoanData(R.drawable.blue_sme_logo, "SME Loan", "Maximum - 300,000,000 MMK ")
        )

        homeRvAdapter = HomeRvAdapter(loanData, this)
        binding.rvHome.adapter = homeRvAdapter
        binding.rvHome.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

    }

    override fun OnClickHomeItems(position: Int) {
        startActivity(Intent(requireContext(), SMELoanActivity::class.java))
    }

}