package com.infotic.loanui.navifragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infotic.loanui.R
import com.infotic.loanui.RepaymentActivity
import com.infotic.loanui.adapters.HomeRv2Adapter
import com.infotic.loanui.adapters.HomeRv2Data
import com.infotic.loanui.adapters.PaymentData
import com.infotic.loanui.adapters.PaymentRvAdapter
import com.infotic.loanui.databinding.FragmentHome2Binding
import com.infotic.loanui.databinding.FragmentPaymentBinding

class HomeFragment2 : Fragment(), HomeRv2Adapter.OnClickHome2Items {

    private lateinit var binding : FragmentHome2Binding
    private var homeRv2Data : List<HomeRv2Data> = listOf()
    private lateinit var homeRv2Adapter: HomeRv2Adapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHome2Binding.inflate(inflater, container, false)
        setUpRv()
        return binding.root
    }

    private fun setUpRv() {
        homeRv2Data = listOf(
            HomeRv2Data(R.drawable.briefcase_logo,"8/7/2023", "329,591 MMK", R.drawable.btn_blue),
            HomeRv2Data(R.drawable.green_agricultural_logo,"15/7/2023", "329,591 MMK", R.drawable.btn_blue),
        )

        homeRv2Adapter = HomeRv2Adapter(homeRv2Data , this)
        binding.rvPayment2.adapter = homeRv2Adapter
    }

    override fun OnClickHome2Items(position: Int) {
        startActivity(Intent(requireContext() , RepaymentActivity::class.java))
    }

}