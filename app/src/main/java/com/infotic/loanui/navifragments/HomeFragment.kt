package com.infotic.loanui.navifragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.infotic.loanui.IndividualLoanActivity
import com.infotic.loanui.R
import com.infotic.loanui.SMELoanActivity
import com.infotic.loanui.adapters.HomeRvAdapter
import com.infotic.loanui.adapters.LoanData
import com.infotic.loanui.adapters.LoanRvAdapter
import com.infotic.loanui.databinding.FragmentHomeBinding

class HomeFragment : Fragment(), HomeRvAdapter.OnClickHomeItems {
    private lateinit var binding : FragmentHomeBinding
    private var loanData : List<LoanData> = listOf()
    private lateinit var homeRvAdapter : HomeRvAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        setUpRv()
        return binding.root
    }

    private fun setUpRv() {
        loanData = listOf(
            LoanData(R.drawable.briefcase_logo, "Individual Loan", "Maximum - 50,000,000 MMK"),
            LoanData(R.drawable.blue_sme_logo, "SME Loan", "Maximum - 300,000,000 MMK ")
        )

        homeRvAdapter = HomeRvAdapter(loanData, this)
        binding.rvHome.adapter = homeRvAdapter
        binding.rvHome.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

    }

    override fun OnClickHomeItems(position: Int) {
        startActivity(Intent(requireContext(), SMELoanActivity::class.java))
    }
}