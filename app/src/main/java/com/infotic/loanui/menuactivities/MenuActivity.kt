package com.infotic.loanui.menuactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infotic.loanui.AboutUsActivity
import com.infotic.loanui.NotificationActivity
import com.infotic.loanui.UpgradeLevel2Activity
import com.infotic.loanui.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.cvUpgrade.setOnClickListener {
            startActivity(Intent(this@MenuActivity, UpgradeLevel2Activity::class.java))
        }

        binding.cvNotification.setOnClickListener {
            startActivity(Intent(this@MenuActivity, NotificationActivity::class.java))
        }

        binding.cvProfile.setOnClickListener {
            startActivity(Intent(this@MenuActivity, EditProfileActivity::class.java))
        }

        binding.cvTerms.setOnClickListener {
            startActivity(Intent(this@MenuActivity, TermsConditionsActivity::class.java))
        }

        binding.cvShareApp.setOnClickListener {
            startActivity(Intent(this@MenuActivity, ShareAppActivity::class.java))
        }

        binding.cvAboutUs.setOnClickListener {
            startActivity(Intent(this@MenuActivity, AboutUsActivity::class.java))
        }

        binding.cvFAQs.setOnClickListener {
            startActivity(Intent(this@MenuActivity, FAQsActivity::class.java))
        }



    }
}