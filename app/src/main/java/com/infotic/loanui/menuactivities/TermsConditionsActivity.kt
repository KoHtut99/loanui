package com.infotic.loanui.menuactivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.infotic.loanui.R

class TermsConditionsActivity : AppCompatActivity() {
    private lateinit var backIcon : ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_conditions)

        backIcon = findViewById(R.id.backIcon)
        backIcon.setOnClickListener { onBackPressed() }
    }
}