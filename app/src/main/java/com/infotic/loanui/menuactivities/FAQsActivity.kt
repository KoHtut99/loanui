package com.infotic.loanui.menuactivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.infotic.loanui.R
import com.infotic.loanui.databinding.ActivityFaqsBinding

class FAQsActivity : AppCompatActivity() {
    private lateinit var binding : ActivityFaqsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFaqsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnShowFirstAnswer.setOnClickListener {
            if (binding.tvA1.visibility == View.GONE){
                binding.tvA1.visibility = View.VISIBLE
                binding.btnShowFirstAnswer.setImageResource(R.drawable.minimize_logo)
            }else{
                binding.tvA1.visibility = View.GONE
                binding.btnShowFirstAnswer.setImageResource(R.drawable.ic_baseline_add_24)
            }
        }

        binding.btnShowSecondAnswer.setOnClickListener {
            if (binding.tvA2.visibility == View.GONE){
                binding.tvA2.visibility = View.VISIBLE
                binding.btnShowSecondAnswer.setImageResource(R.drawable.minimize_logo)
            }else{
                binding.tvA2.visibility = View.GONE
                binding.btnShowSecondAnswer.setImageResource(R.drawable.ic_baseline_add_24)
            }
        }

        binding.btnShowThirdAnswer.setOnClickListener {
            if (binding.tvA3.visibility == View.GONE){
                binding.tvA3.visibility = View.VISIBLE
                binding.btnShowThirdAnswer.setImageResource(R.drawable.minimize_logo)
            }else{
                binding.tvA3.visibility = View.GONE
                binding.btnShowThirdAnswer.setImageResource(R.drawable.ic_baseline_add_24)
            }
        }

        binding.btnShowFourthAnswer.setOnClickListener {
            if (binding.tvA4.visibility == View.GONE){
                binding.tvA4.visibility = View.VISIBLE
                binding.btnShowSecondAnswer.setImageResource(R.drawable.minimize_logo)
            }else{
                binding.tvA4.visibility = View.GONE
                binding.btnShowFourthAnswer.setImageResource(R.drawable.ic_baseline_add_24)
            }
        }
    }
}