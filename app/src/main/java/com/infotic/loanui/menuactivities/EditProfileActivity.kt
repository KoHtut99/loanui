package com.infotic.loanui.menuactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.cast.framework.media.ImagePicker
import com.infotic.loanui.LoginActivity
import com.infotic.loanui.UpgradeLevel2Activity
import com.infotic.loanui.databinding.ActivityEditProfileBinding

class EditProfileActivity : AppCompatActivity() {
    private lateinit var binding : ActivityEditProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.ivEdit.setOnClickListener {
            uploadImage()
        }

        binding.btnLogout.setOnClickListener {
            startActivity(Intent(this@EditProfileActivity, LoginActivity::class.java))
        }

        binding.btnUpgradeLevel.setOnClickListener {
            startActivity(Intent(this@EditProfileActivity, UpgradeLevel2Activity::class.java))
        }


    }

    private fun uploadImage() {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        intent.type = "image/*"
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            binding.ivProfile.setImageURI(data?.data)
        }
    }
}