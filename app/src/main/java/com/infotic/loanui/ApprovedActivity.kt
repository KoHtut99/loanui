package com.infotic.loanui

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.Button
import com.infotic.loanui.databinding.ActivityApprovedBinding
import com.infotic.loanui.databinding.ActivityPaymentSuccessFormBinding

class ApprovedActivity : AppCompatActivity() {
    private lateinit var binding : ActivityApprovedBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityApprovedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backIcon.setOnClickListener { onBackPressed() }

        binding.btnWithdraw.setOnClickListener {
            val i = Intent(this@ApprovedActivity, WithdrawLoadingActivity::class.java)
            i.getStringExtra("key2")
            startActivity(i)
        }
    }
}