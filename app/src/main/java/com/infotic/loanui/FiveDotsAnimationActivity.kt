package com.infotic.loanui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.infotic.loanui.databinding.ActivityFiveDotsAnimationBinding
import com.infotic.loanui.loanforms.ThirdLoanFormActivity

class FiveDotsAnimationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFiveDotsAnimationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFiveDotsAnimationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler().postDelayed( {
            val intent = Intent(this@FiveDotsAnimationActivity, ThirdLoanFormActivity::class.java)
            intent.putExtra("key", "ShowDialog")
            startActivity(intent)
        }, 2000)

    }
}
